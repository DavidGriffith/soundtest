# This Makefile is designed specifically for building programs written
# in Inform.  Feel free to adopt and adapt it for your own purposes.
#

MAKE = make

VERSION = r0
BINNAME = soundtest
EXTENSION = .z5
BLORB_EXTENSION = .zblorb

INFORM = inform
BLORB = pblorb.pl

NODEBUG = -~D~S
DEBUG = -D

GLULX = -G
SWITCHES = -iecd2

DISTNAME = $(BINNAME)-$(VERSION)
distdir = $(DISTNAME)
DIST_EXTENSION = .tar.gz

BLURBFILE = $(BINNAME).blurb
BLORB_CONST = blurb.inf

$(BINNAME): blurb
	$(INFORM) $(SWITCHES) $(DEBUG) $(BINNAME).inf $(BINNAME)$(EXTENSION)
	$(MAKE) blorb

nodebug: blurb
	$(INFORM) $(SWITCHES) $(NODEBUG) $(BINNAME).inf $(BINNAME)$(EXTENSION)

blurb:
	$(BLORB) -i $(BLURBFILE) -o $(BLORB_CONST)

blorb: blurb
	$(BLORB) $(BLURBFILE) -o $(BINNAME)$(BLORB_EXTENSION)


dist:
ifneq ($(and $(wildcard .git),$(shell which git)),)
	git archive --format=tgz --prefix $(BINNAME)-$(VERSION)/ HEAD -o $(BINNAME)-$(VERSION).tar.gz
else
	@echo "Not in a git repository or git command not found.  Cannot make a tarball."
endif
	@echo
	@echo "$(distdir)$(DIST_EXTENSION) created"
	@echo

clean:
	rm -f $(BINNAME)$(EXTENSION)
	rm -f $(BINNAME)$(BLORB_EXTENSION)
	rm -f $(BLORB_CONST)


distclean:
	-rm -rf $(distdir)
	-rm -f $(distdir)$(DIST_EXTENSION)

help:
	@echo "Makefile for Inform compilation"
	@echo "Targets:"
	@echo "  $(BINNAME)	Build the game."
	@echo "  nodebug	Build the game without debugging code."
	@echo "  blorb		Build Blorb file."
	@echo "  clean		Get rid of temporary files."
	@echo "  help		This help message."
	@echo "Default target is \"$(BINNAME)\""
